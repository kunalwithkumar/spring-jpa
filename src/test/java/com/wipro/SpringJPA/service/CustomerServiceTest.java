package com.wipro.SpringJPA.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import com.wipro.SpringJPA.repository.ICustomerRepo;

import lombok.extern.slf4j.Slf4j;

@ExtendWith(MockitoExtension.class)
@Slf4j
class CustomerServiceTest {

	@Mock
	ICustomerRepo repo;

	@InjectMocks
	CustomerServiceImpl service;

	@BeforeEach
	public void init() {

		log.info("before All called");
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void findAllTest() {

		log.info("inside findAllTest");
		assertEquals(repo.findAll(), service.findAll());
	}

}
