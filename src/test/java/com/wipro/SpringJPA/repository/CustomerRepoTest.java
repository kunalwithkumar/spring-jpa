package com.wipro.SpringJPA.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.wipro.SpringJPA.entity.Customer;

import lombok.extern.slf4j.Slf4j;

@SpringBootTest
@Slf4j
public class CustomerRepoTest {

	@Autowired
	private ICustomerRepo repo;

	@Test
	public void findAllTest() {

		log.info("find all test called");

		Customer customer = new Customer();

		customer.setCustomerId(1L);
		customer.setName("User 1");
		repo.save(customer);

		Customer cust = repo.findById(1L).get();
		assertNotNull(cust);
	}

}
