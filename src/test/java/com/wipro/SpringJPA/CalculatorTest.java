package com.wipro.SpringJPA;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CalculatorTest {

	public static Calculator cal;

	@BeforeAll
	public static void beforeAll() {

		System.out.println("object creation started");
		cal = new Calculator();
		System.out.println("object created");
	}

	@Test
	void testAdd() {
		System.out.println("add method test called");
		assertEquals(9, cal.add(5, 4));
	}

	@Test
	void testSub() {

		assertEquals(5, cal.sub(10, 5));

	}

	@Test
	void testMul() {

		assertEquals(20, cal.mul(10, 2));

	}

};