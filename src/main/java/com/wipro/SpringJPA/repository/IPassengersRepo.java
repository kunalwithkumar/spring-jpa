package com.wipro.SpringJPA.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.wipro.SpringJPA.entity.Page;

public interface IPassengersRepo extends JpaRepository<Page, Long> {

	@Query("from Page where book.id =1")
	public List<Page> getPage(Long id);

	public List<Page> findByBookId(Long id);

}
