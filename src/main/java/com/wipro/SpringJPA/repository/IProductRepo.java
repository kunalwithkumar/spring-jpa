package com.wipro.SpringJPA.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.wipro.SpringJPA.entity.Product;

@Repository // optional
public interface IProductRepo extends JpaRepository<Product, Long> {

	public List<Product> findByProductName(String productName);

	public List<Product> findByPrice(Long price);

	// to get the product List greater than
	public List<Product> findByPriceGreaterThan(Long price);

	public List<Product> findByPriceLessThan(Long price);

	public List<Product> findByManufactureDateGreaterThan(Date manufactureDate);

//	@Query("insert into Product values(?1,?2,?3)")
//	public Product insertProduct(Long id,String name,Long price);

//	@Query("from Product where customer.customerId =:1")
//	public List<Product> findByCustomerId(Long id);

	@Query("select p from Product p where productName = ?1 and price = ?2")
	public List<Product> getByProductNameAndSalary(String name, Long price);

}
