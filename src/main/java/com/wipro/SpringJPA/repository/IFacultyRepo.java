package com.wipro.SpringJPA.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wipro.SpringJPA.entity.Faculty;

public interface IFacultyRepo extends JpaRepository<Faculty, Long> {

	List<Faculty> findByNameContaining(String name);

}
