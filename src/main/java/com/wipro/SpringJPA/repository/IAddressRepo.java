package com.wipro.SpringJPA.repository;

import org.springframework.data.repository.CrudRepository;

import com.wipro.SpringJPA.entity.Address;

public interface IAddressRepo extends CrudRepository<Address, Long> {

}
