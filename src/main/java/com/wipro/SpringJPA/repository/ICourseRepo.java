package com.wipro.SpringJPA.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wipro.SpringJPA.entity.Course;

public interface ICourseRepo extends JpaRepository<Course, Long> {

	List<Course> findByTitleContaining(String title);

	List<Course> findByFeeLessThan(double fee);
}
