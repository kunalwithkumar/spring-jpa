package com.wipro.SpringJPA.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wipro.SpringJPA.entity.Book;

public interface IFlightRepo extends JpaRepository<Book, Long> {

}
