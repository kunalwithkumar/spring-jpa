package com.wipro.SpringJPA.repository;

import org.springframework.data.repository.CrudRepository;

import com.wipro.SpringJPA.entity.Student;

public interface IStudentRepo extends CrudRepository<Student, Long> {

}
