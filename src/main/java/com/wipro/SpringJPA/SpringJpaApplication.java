package com.wipro.SpringJPA;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringJpaApplication.class, args);
	}

//	@Bean
//	public CommandLineRunner mappingDemo(IStudentRepo studentRepo, IAddressRepo addressRepo) {
//
//		return args -> {
//			Student student = new Student();
//
//			Address address = new Address();
//			address.setCity("Hyd");
//			address.setCountry("India");
//			address.setState("Telengana");
//
//			addressRepo.save(address);
//
//			student.setName("kunal");
//			student.setEmail("kunal@gmail.com");
//			student.setAddress(address);
//			studentRepo.save(student);
//		};
//	}

//	@Bean
//	@Transactional
//	public CommandLineRunner mappingDemo(IFlightRepo bookRepository, IPassengersRepo pageRepository) {
//		return args -> {
//
//			// create a new book
//			Book book = new Book("Java 101", "John Doe", "123456");
//
//			// save the book
//			bookRepository.save(book);
//
//			// create and save new pages
//			pageRepository.save(new Page(1, "Introduction contents", "Introduction", book));
//			pageRepository.save(new Page(65, "Java 8 contents", "Java 8", book));
//			pageRepository.save(new Page(95, "Concurrency contents", "Concurrency", book));
//
//		};
//	}

//	@Bean
//	public CommandLineRunner mappingDemo(IFacultyRepo studentRepository, ICourseRepo courseRepository) {
//		return args -> {
//
//			// create a student
//			Faculty student = new Faculty("John Doe", 15, "8th");
//
//			// save the student
//			studentRepository.save(student);
//
//			// create three courses
//			Course course1 = new Course("Machine Learning", "ML", 12, 1500);
//			Course course2 = new Course("Database Systems", "DS", 8, 800);
//			Course course3 = new Course("Web Basics", "WB", 10, 0);
//
//			// save courses
//			courseRepository.saveAll(Arrays.asList(course1, course2, course3));
//
//			// add courses to the student
//			student.getCourses().addAll(Arrays.asList(course1, course2, course3));
//
//			// update the student
//			studentRepository.save(student);
//		};
//	}

}
