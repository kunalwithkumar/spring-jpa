package com.wipro.SpringJPA.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.wipro.SpringJPA.entity.Customer;
import com.wipro.SpringJPA.repository.ICustomerRepo;

@Service
public class CustomerServiceImpl implements ICustomerService {

	@Autowired
	ICustomerRepo customerRepo;

//	@Autowired
//	EntityManager entityManager;

	// sorting
	@Override
	public List<Customer> findAll(String name) {
		return customerRepo.findAll(Sort.by(name));
	}

	@Override
	public List<Customer> findAll() {
		return customerRepo.findAll();
	}

	@Override
	public Customer save(Customer customer) {
		return customerRepo.save(customer);
	}

	// pagination
	@Override
	public List<Customer> getPaged() {
		Pageable page = PageRequest.of(0, 2);
		return customerRepo.findAll(page).toList();
	}

	@Override
	public List<Customer> saveAll(List<Customer> customer) {
		return customerRepo.saveAllAndFlush(customer);
	}

}
