package com.wipro.SpringJPA.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.wipro.SpringJPA.entity.Product;

public interface IProduct {

	public Product addProduct(Product product);

	// public Product insertProduct(Long id,String name,Long price);

	public Product updateProduct(Product product);

	public Product getProductByid(Long id);

	public ResponseEntity<String> deleteProduct(Long id);

	public List<Product> getAllProducts();

	public List<Product> findByProductName(String productName);

	public List<Product> findByPrice(Long price);

	public List<Product> findByPriceGreaterThan(Long price);

	public List<Product> findByPriceLessThan(Long price);

	public List<Product> findByManufactureDateGreaterThan(String manufactureDate);

	public List<Product> getByProductNameAndSalary(String name, Long price);

	public void name();

}
