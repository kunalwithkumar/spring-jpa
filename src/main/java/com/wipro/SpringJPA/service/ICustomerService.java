package com.wipro.SpringJPA.service;

import java.util.List;

import com.wipro.SpringJPA.entity.Customer;

public interface ICustomerService {

	public List<Customer> findAll();

	public List<Customer> findAll(String name);

	public Customer save(Customer customer);

	public List<Customer> getPaged();

	public List<Customer> saveAll(List<Customer> customer);

}
