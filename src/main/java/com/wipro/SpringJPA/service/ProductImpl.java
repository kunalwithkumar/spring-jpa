package com.wipro.SpringJPA.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.wipro.SpringJPA.entity.Page;
import com.wipro.SpringJPA.entity.Product;
import com.wipro.SpringJPA.repository.IPassengersRepo;
import com.wipro.SpringJPA.repository.IProductRepo;

@Service
public class ProductImpl implements IProduct {

	@Autowired
	IProductRepo productRepo;
	@Autowired
	IPassengersRepo pageRepository;

	@Override
	public Product addProduct(Product product) {
		return productRepo.save(product);
	}

	@Override
	public Product updateProduct(Product product) {
		return productRepo.save(product);
	}

	@Override
	public Product getProductByid(Long id) {
		return productRepo.findById(id).get();
		// productRepo.findById(id).orElse(new Product());
	}

	@Override
	public ResponseEntity<String> deleteProduct(Long id) {
		productRepo.deleteById(id);
		return new ResponseEntity<String>("Deleted", HttpStatus.OK);
	}

	@Override
	public List<Product> getAllProducts() {
		return productRepo.findAll();
	}

	@Override
	public List<Product> findByProductName(String productName) {
		return productRepo.findByProductName(productName);
	}

	@Override
	public List<Product> findByPrice(Long price) {
		return productRepo.findByPrice(price);
	}

	@Override
	public List<Product> findByPriceGreaterThan(Long price) {
		return productRepo.findByPriceGreaterThan(price);
	}

	@Override
	public List<Product> findByManufactureDateGreaterThan(String manufactureDate) {
		return productRepo.findAll(Sort.by(manufactureDate));

		// return productRepo.findByManufactureDateGreaterThan(manufactureDate);
	}

	@Override
	public List<Product> findByPriceLessThan(Long price) {
		return productRepo.findByPriceLessThan(price);
	}

	@Override
	public List<Product> getByProductNameAndSalary(String name, Long price) {
		return productRepo.getByProductNameAndSalary(name, price);
	}

//	@Override
//	public Product insertProduct(Long id, String name, Long price) {
//		// TODO Auto-generated method stub
//		return productRepo.insertProduct(id, name, price);
//	}

	@Override
	// @Transactional
	public void name() {

		List<Page> page = new ArrayList<Page>();
		// List<Page> page = pageRepository.getPage(1L);
		page = pageRepository.findByBookId(1L);
		System.out.println("Page details is :" + page);
	}

	public String hello() {
		return "hello";
	}
}
