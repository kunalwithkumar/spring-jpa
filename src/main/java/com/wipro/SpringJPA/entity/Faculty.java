package com.wipro.SpringJPA.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "faculty")
public class Faculty implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;
	private int age;
	private String grade;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinTable(name = "faculty_courses", joinColumns = {
			@JoinColumn(name = "faculty_id", referencedColumnName = "id", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "course_id", referencedColumnName = "id", nullable = false, updatable = false) })
	private Set<Course> courses = new HashSet<>();

	public Faculty() {
	}

	public Faculty(String name, int age, String grade) {
		this.name = name;
		this.age = age;
		this.grade = grade;
	}

	// getters and setters, equals(), toString() .... (omitted for brevity)
}
