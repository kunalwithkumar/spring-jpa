package com.wipro.SpringJPA.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "courses")
public class Course implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String title;
	private String abbreviation;
	private int modules;
	private double fee;

	@ManyToMany(mappedBy = "courses", fetch = FetchType.LAZY)
	private Set<Faculty> students = new HashSet<>();

	public Course() {
	}

	public Course(String title, String abbreviation, int modules, double fee) {
		this.title = title;
		this.abbreviation = abbreviation;
		this.modules = modules;
		this.fee = fee;
	}

	// getters and setters, equals(), toString() .... (omitted for brevity)
}