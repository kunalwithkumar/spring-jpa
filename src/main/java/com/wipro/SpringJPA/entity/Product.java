package com.wipro.SpringJPA.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product") // optional
public class Product implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "product_id") // @Colume is optional
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String productName;

	private Long price;

	@JoinColumn(name = "cust_id")
	@OneToOne(cascade = CascadeType.ALL)
	private Customer customer;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date manufactureDate;

}
