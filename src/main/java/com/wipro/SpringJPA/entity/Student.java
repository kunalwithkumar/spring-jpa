package com.wipro.SpringJPA.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "student")
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long studentId;

	private String name;
	private String email;

	@OneToOne // (mappedBy = "student")
	@JoinColumn(name = "addressId") // userDefined name for the colume
	private Address address;

}
