package com.wipro.SpringJPA.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.SpringJPA.entity.Customer;
import com.wipro.SpringJPA.service.ICustomerService;

@RestController
@RequestMapping("/customer")
public class CostomerController {

	@Autowired
	ICustomerService service;

	@PostMapping("/add")
	public Customer save(@RequestBody Customer customer) {
		return service.save(customer);
	}

	@PostMapping("/addAll")
	public List<Customer> saveAll(@RequestBody List<Customer> customers) {
		return service.saveAll(customers);
	}

	@GetMapping("/get/{name}")
	public List<Customer> findAll(@PathVariable String name) {
		return service.findAll(name);
	}

	@GetMapping("/paged")
	public List<Customer> getPaged() {
		return service.getPaged();
	}

}
