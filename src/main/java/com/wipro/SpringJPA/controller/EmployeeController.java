package com.wipro.SpringJPA.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.SpringJPA.service.EmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService service;

	@GetMapping("/hello")
	// @RequestMapping(value = "hello", method = RequestMethod.GET)
	// @ResponseBody
	public String hello() {

		return "hello";
	}

	@GetMapping("/getEmployee")
	public String getRepo() {
		return service.getrepo();
	}

}
