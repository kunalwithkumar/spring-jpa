package com.wipro.SpringJPA.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.SpringJPA.entity.Product;
import com.wipro.SpringJPA.service.IProduct;

@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired
	IProduct service;

	@PostMapping("/addProduct")
	public Product addProduct(@RequestBody Product product) {

		return service.addProduct(product);
	}

	@PutMapping("/updateProduct")
	public Product updateProduct(@RequestBody Product product) {
		return service.updateProduct(product);
	}

	@GetMapping("/getProduct/{id}")
	public Product getProductByid(@PathVariable Long id) {

		service.name();

		return service.getProductByid(id);
	}

	@DeleteMapping("/deleteProduct/{id}")
	public ResponseEntity<String> deleteProduct(@PathVariable Long id) {
		return service.deleteProduct(id);
	}

	@GetMapping("/getAllProduct")
	public List<Product> getAllProducts() {
		return service.getAllProducts();
	}

	@GetMapping("/findByProductName/{pname}")
	public List<Product> findByProductName(@PathVariable String pname) {
		return service.findByProductName(pname);
	}

	@GetMapping("/findByPrice/{price}")
	public List<Product> findByPrice(@PathVariable Long price) {
		return service.findByPrice(price);
	}

	@GetMapping("/findByPriceGreaterThan/{price}")
	public List<Product> findByPriceGreaterThan(@PathVariable Long price) {
		return service.findByPriceGreaterThan(price);
	}

	@GetMapping("/findByPriceLessThan/{price}")
	public List<Product> findByPriceLessThan(@PathVariable Long price) {
		return service.findByPriceLessThan(price);
	}

	// have a look in this method how to do this
	@GetMapping("/findByManufactureDateLessThan/{manufactureDate}")
	public List<Product> findByManufactureDateGreaterThan(@PathVariable String manufactureDate) {
		return service.findByManufactureDateGreaterThan(manufactureDate);
	}

//	@PostMapping("/insert/{id}/{}/{}")
//	public Product insertProduct(@PathVariable Long id,@PathVariable String name,@PathVariable Long price) {
//
//	}

	@GetMapping("/getByProductNameAndSalary/{name}/{price}")
	public List<Product> getByProductNameAndSalary(@PathVariable String name, @PathVariable Long price) {
		return service.getByProductNameAndSalary(name, price);
	}
}
